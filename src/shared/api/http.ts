import axios, { AxiosError, AxiosResponse } from 'axios'

export const http = axios.create({
  baseURL: 'https://api.punkapi.com/v2',
})

const interceptors = {
  onSuccess: <T>(response: AxiosResponse<T>): T => response.data,
  onError: (error: AxiosError) => Promise.reject(error),
}

http.interceptors.response.use(interceptors.onSuccess, interceptors.onError)
