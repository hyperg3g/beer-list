import { BeerResponse } from '../models'
import { http } from './http'

interface GetBeerListQueryParams extends Record<string, string> {
  page: string
  per_page: string
}

export function getBeersList(params?: GetBeerListQueryParams) {
  const queryParams = params ? `?${new URLSearchParams(params).toString()}` : ''

  return http.get<BeerResponse[], BeerResponse[]>(`/beers${queryParams}`)
}
