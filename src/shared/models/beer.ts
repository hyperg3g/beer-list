export interface Beer {
  id: number
  name: string
  description: string
  imageUrl: string
}

export interface BeerResponse {
  id: number
  name: string
  description: string
  image_url: string
}
