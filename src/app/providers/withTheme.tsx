import { CssBaseline, createTheme } from '@mui/material'
import ThemeProvider from '@mui/material/styles/ThemeProvider'

export function withTheme(component: () => React.ReactNode) {
  const theme = createTheme({
    palette: {
      mode: 'dark',
    },
  })

  return () => (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      {component()}
    </ThemeProvider>
  )
}
