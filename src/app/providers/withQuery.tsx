import { QueryCache, QueryClient, QueryClientProvider } from 'react-query'

export function withQuery(component: () => React.ReactNode) {
  const queryClient = new QueryClient({
    queryCache: new QueryCache({
      onError: error => console.error(`Something went wrong: ${(error as Error).message}`),
    }),
  })

  return () => <QueryClientProvider client={queryClient}>{component()}</QueryClientProvider>
}
