import routes from '@/pages/routes'
import { ReactLocation, Router } from 'react-location'

export function withRouter(component: () => React.ReactNode) {
  const location = new ReactLocation()

  return () => (
    <Router location={location} routes={routes}>
      {component()}
    </Router>
  )
}
