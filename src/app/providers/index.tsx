import compose from 'compose-function'
import { withQuery } from './withQuery'
import { withRouter } from './withRouter'
import { withTheme } from './withTheme'

export const withAppProviders = compose(withQuery, withRouter, withTheme)
