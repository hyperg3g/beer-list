import '@fontsource/roboto/300.css'
import '@fontsource/roboto/400.css'
import '@fontsource/roboto/500.css'
import '@fontsource/roboto/700.css'

import { Outlet } from 'react-location'
import { withAppProviders } from './providers'
import './App.module.scss'

const App = withAppProviders(() => {
  return <Outlet />
})

export default App
