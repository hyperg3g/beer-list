import { getBeersList } from '@/shared/api'
import { useInfiniteQuery } from 'react-query'
import camelize from 'camelize-ts'
import { Beer } from '@/shared/models'

const GET_BEERS_QUERY = 'beers'
const PER_PAGE_LIMIT = 25

export function useGetBeersQuery() {
  return useInfiniteQuery(
    [GET_BEERS_QUERY],
    ({ pageParam = 1 }) => getBeersList({ page: pageParam.toString(), per_page: PER_PAGE_LIMIT.toString() }),
    {
      select: data => {
        return { ...data, pages: camelize(data.pages) as Beer[][] }
      },
      getNextPageParam: lastPage => {
        const hasNextPage = lastPage.length === PER_PAGE_LIMIT
        const nextPage = hasNextPage ? lastPage[PER_PAGE_LIMIT - 1].id / PER_PAGE_LIMIT + 1 : undefined

        return nextPage
      },
    }
  )
}
