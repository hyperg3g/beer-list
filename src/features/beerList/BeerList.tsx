import BeerCard from '@/entities/beerCard'
import { useGetBeersQuery } from './useGetBeersQuery'
import { Grid } from '@mui/material'
import { SearchStatus } from './ui'
import { useInView } from 'react-intersection-observer'
import { useEffect } from 'react'

function BeerList() {
  const { ref, inView } = useInView()
  const { data: beers, isLoading, isError, fetchNextPage, hasNextPage } = useGetBeersQuery()

  useEffect(() => {
    inView && fetchNextPage()
  }, [fetchNextPage, inView])

  return (
    <Grid container spacing={{ xs: 2, md: 3 }} columns={{ xs: 4, sm: 8, md: 12 }} alignItems="stretch">
      {isLoading && <SearchStatus text="Searching for beer! 🤔" />}
      {(beers?.pages.length === 0 || isError) && <SearchStatus text="Can't find any beer... 🥲" />}
      {beers && (
        <>
          <SearchStatus text="I found something! 🍻" />
          {beers.pages.map(page => {
            return page.map((beer, index) => {
              return (
                <Grid key={`${index}_${beer.id}`} item xs={4} sm={4} md={4}>
                  <BeerCard beer={beer} />
                </Grid>
              )
            })
          })}
        </>
      )}
      <SearchStatus text={`${hasNextPage ? 'Loading more beer 😏' : 'This is all the beer I found 🤷'}`} ref={ref} />
    </Grid>
  )
}

export default BeerList
