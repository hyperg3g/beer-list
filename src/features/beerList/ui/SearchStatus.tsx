import { Grid, Typography } from '@mui/material'
import { RefObject, forwardRef } from 'react'

interface Props {
  text: string
}

export const SearchStatus = forwardRef(({ text }: Props, ref) => {
  return (
    <Grid item component="div" xs={4} sm={8} md={12} ref={ref as RefObject<HTMLDivElement>}>
      <Typography variant="h4" textAlign="center">
        {text}
      </Typography>
    </Grid>
  )
})
