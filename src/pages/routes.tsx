import { Route, DefaultGenerics } from 'react-location'
import IndexPage from './index'

export enum Paths {
  INDEX = '/',
}

const routes: Route<DefaultGenerics>[] = [
  {
    path: Paths.INDEX,
    element: <IndexPage />,
  },
]

export default routes
