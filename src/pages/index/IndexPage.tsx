import BeerList from '@/features/beerList'
import { AppBar, Container, Toolbar, Typography } from '@mui/material'

function IndexPage() {
  return (
    <>
      <AppBar position="static" component="nav">
        <Toolbar>
          <Typography variant="h6">Punk API Beer List</Typography>
        </Toolbar>
      </AppBar>

      <Container sx={{ p: 4 }}>
        <BeerList />
      </Container>
    </>
  )
}

export default IndexPage
