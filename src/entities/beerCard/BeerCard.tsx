import { Beer } from '@/shared/models'
import { Card, CardActionArea, Typography, CardMedia, CardContent, CardActions, Button } from '@mui/material'

interface Props {
  beer: Beer
}

function BeerCard({ beer }: Props) {
  return (
    <Card sx={{ maxWidth: 345, display: 'flex', flexDirection: 'column', height: '100%' }}>
      <CardActionArea>
        <CardMedia component="img" height={150} image={beer.imageUrl} alt={beer.name} />
      </CardActionArea>

      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          {beer.name}
        </Typography>

        <Typography variant="body2" color="text.secondary">
          {beer.description}
        </Typography>
      </CardContent>

      <CardActions disableSpacing={true} sx={{ mt: 'auto' }}>
        <Button size="small">Learn more</Button>
      </CardActions>
    </Card>
  )
}

export default BeerCard
